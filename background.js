// var i = 0;

// function timedCount() {
//     i = i + 1;
//     postMessage(i);
//     if (i < 10) {
//         setTimeout("timedCount()", 500);
//     }
// }

// timedCount();

function resolveAfter2Seconds() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(0);
    }, 3000);
  });
}

async function goTime() {
    postMessage("Started");
  var a = await resolveAfter2Seconds();
     postMessage("LocalCache");
   var b = await resolveAfter2Seconds();
     postMessage("Validated");
}

goTime();